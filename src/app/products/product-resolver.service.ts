import {Injectable} from "@angular/core";
import {ProductResolved} from "./product";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable, of} from "rxjs";
import {ProductService} from "./product.service";
import {catchError, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ProductResolver implements Resolve<ProductResolved> {

  constructor(private productService: ProductService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ProductResolved> {
    const id = route.paramMap.get('id');
    const resolvedProduct: ProductResolved = {product: null, error: null};

    if (isNaN(+id)) {
      resolvedProduct.error = `${id} is not a valid id`;
    } else {
      return this.productService.getProduct(+id).pipe(
        map(product => {return {product: product};}),
        catchError(err => {return of({product: null, error: err})})
      );
    }
  }
}
