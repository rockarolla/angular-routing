import {Component, OnInit} from '@angular/core';

import { MessageService } from '../../messages/message.service';

import {Product, ProductResolved} from '../product';
import { ProductService } from '../product.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  pageTitle = 'Product Edit';
  errorMessage: string;

  private isDataValid: {[key: string]: boolean} = {
    info: true,
    tags: true
  };

  private currentProduct: Product;
  private originalProduct: Product;

  constructor(private productService: ProductService,
              private messageService: MessageService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {

    this.route.data.subscribe(data => {
      const productData: ProductResolved = data['resolvedProductData'];
      this.errorMessage = productData.error;
      this.onProductRetrieved(productData.product);
    });
  }

  get product(): Product {
    return this.currentProduct;
  }

  set product(value: Product) {
    this.currentProduct = value;
    this.originalProduct = {...value};
  }

  get isDirty(): boolean {
    return JSON.stringify(this.originalProduct) !== JSON.stringify(this.currentProduct);
  }

  onProductRetrieved(product: Product): void {
    this.product = product;

    if (!this.product) {
      this.pageTitle = 'No product found';
    } else {
      if (this.product.id === 0) {
        this.pageTitle = 'Add Product';
      } else {
        this.pageTitle = `Edit Product: ${this.product.productName}`;
      }
    }
  }

  deleteProduct(): void {
    if (this.product.id === 0) {
      // Don't delete, it was never saved.
      this.onSaveComplete(`${this.product.productName} was deleted`);
    } else {
      if (confirm(`Really delete the product: ${this.product.productName}?`)) {
        this.productService.deleteProduct(this.product.id).subscribe({
          next: () => this.onSaveComplete(`${this.product.productName} was deleted`),
          error: err => this.errorMessage = err
        });
      }
    }
  }

  saveProduct(): void {
    if (this.isValid()) {
      if (this.product.id === 0) {
        this.productService.createProduct(this.product).subscribe({
          next: () => this.onSaveComplete(`The new ${this.product.productName} was saved`),
          error: err => this.errorMessage = err
        });
      } else {
        this.productService.updateProduct(this.product).subscribe({
          next: () => this.onSaveComplete(`The updated ${this.product.productName} was saved`),
          error: err => this.errorMessage = err
        });
      }
    } else {
      this.errorMessage = 'Please correct the validation errors.';
    }
  }

  onSaveComplete(message?: string): void {
    if (message) {
      this.messageService.addMessage(message);
    }
    this.reset();
    this.router.navigate(['/products']);
  }

  cancelEdit() {
    if (this.product.id != 0) {
      this.router.navigate(['/products', this.product.id]);
    } else {
      this.router.navigate(['/products']);
    }
  }

  validate(): void {
    this.isDataValid = {
      info: true,
      tags: true
    };

    if (!this.product.productName || this.product.productName.length < 3) {
      this.isDataValid.info = false;
    }
    if (!this.product.productCode) {
      this.isDataValid.info = false;
    }

    if (!this.product.category || this.product.category.length < 3) {
      this.isDataValid.tags = false;
    }
  }

  isValid(path?: string): boolean {
    this.validate();
    if (path) {
      return this.isDataValid[path];
    } else {
      return this.isDataValid &&
        Object.keys(this.isDataValid).every(tab => this.isDataValid[tab] === true);
    }
  }

  reset(): void {
    this.isDataValid = null;
    this.product = this.product;
  }
}
