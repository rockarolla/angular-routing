import {Component} from '@angular/core';

import {AuthService} from './user/auth.service';
import {Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from "@angular/router";
import {MessageService} from "./messages/message.service";

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  pageTitle = 'Acme Product Management';
  loading = true;

  get isLoggedIn(): boolean {
    return this.authService.isLoggedIn;
  }

  get userName(): string {
    if (this.authService.currentUser) {
      return this.authService.currentUser.userName;
    }
    return '';
  }

  constructor(private authService: AuthService, private router: Router, private messageService: MessageService) {
    this.router.events.subscribe(event => {
      this.checkRouterEvent(event);
    });
  }

  logOut(): void {
    this.authService.logout();
    this.router.navigateByUrl('/welcome');
    console.log('Log out');
  }

  private checkRouterEvent(event: Event) {
    if (event instanceof NavigationStart) {
      this.loading = true;
    } else if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
      this.loading = false;
    }
  }

  showMessages(): void {
    this.messageService.toggleMessages();
    this.router.navigate([{outlets: {messages: 'messages'}}]);
  }

  hideMessages(): void {
    this.messageService.toggleMessages();
    this.router.navigate([{outlets: {messages: null}}]);
  }

  showingMessages(): boolean {
    return this.messageService.showingMessages();
  }
}
